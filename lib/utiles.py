#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# Jota, versión: 13072021
# USO FREE
import time
import urllib.request as urllib2
from wireless import Wireless
from datetime import datetime
import requests
import smtplib
import paho.mqtt.client as mqtt
import json
import subprocess
import os.path 
        
"""
Clase Utilitaria con funciones de uso general
"""
class Commons:

    file_dir='config.json'
    
    """
    Retorna el valor definido en config del parametro asociado a un item o elemento
    """
    def configuration (self,elemento,parametro):
        elem=self.load_config(self.file_dir,elemento)
        return (elem.get(parametro))
    """
    cambia el valor de un parametro del archivo json de configuracion
    """
    def write_config(self,file,item,parametro,new):
        if (os.path.isfile(file)):
            with open(file, 'r') as f:
                config = json.load(f)
                f.close()
            config[item][parametro]=new
            if (os.path.isfile(file)):
                with open(file, 'w') as f:
                    f.write(json.dumps(config))
                    f.close()
                return (True)
            else:
                return (False)
    
    def write_wifi(self,ap,clave):
        file1='/etc/wpa_supplicant/wpa_supplicant.conf'
        file2='wpa_supplicant.new'
        if (os.path.isfile(file1)):
            with open(file1, 'r') as f:
                conte = f.read()
                f.close()
        text= "\n"+"network={"+"\n"+"        ssid="+"\""+ap+"\""+"\n"+"        psk="+"\""+clave+"\""+"\n"+"        key_mgmt=WPA-PSK"+"\n"+"}"+"\n"
        with open(file2, 'w') as f:
            f.write(conte+text)
            f.close()
        self.run_command("sudo cp "+file1+ " /etc/wpa_supplicant/wpa_supplicant.old")
        self.run_command("sudo cp "+file2+ " /etc/wpa_supplicant/wpa_supplicant.conf")
        os.remove(file2)
        return(True)
            
    def load_config(self,file,device):
        """
       Carga configuracion desde: file_dir , retorna JSON
        """   
        if (os.path.isfile(file)):
            with open(file, 'r') as f:
                config = json.load(f)
                aux = config.get(device)
            return (aux)
        else:
            #self.log.write_log("error archivo config.json")
            return (False)
    """
    guarda medicion para envio futuro cuando haya wifi
    """ 
    def write_data(self,data):
        name_path_send=self.configuration('General','send_path')
        nameFile= str(name_path_send)+self.format_string()+".txt"
        if (os.path.isfile(nameFile)):
            archivo=open(nameFile,"a")
        else:
            archivo=open(nameFile,"w")
        #aux=json.dumps(data)
        archivo.write(data+"\n")
        archivo.close()
        return True
    """
    guarda errores en log  e imprime mensajes para debug
    """
    def write_log(self,data):
        name_file=self.configuration('General','log_file')
        name_path=self.configuration('General','log_path')
        if (os.path.isfile(name_path+name_file)):
            archivo=open(name_path+name_file,"a")           
        else:
            archivo=open(name_path+name_file,"w")
        # para hacer debug por pantalla descomentar linea siguiente 
        # print (self.format_time()+" "+data+"\n")
        archivo.write(self.format_time()+" "+data+"\n")
        archivo.close()        
        return False
    """
    ejecuta comandos del sistema consola , incluye comandos con sudo
    """
     
    def run_command(self,cmd):
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
        output, err = proc.communicate()
        del proc
        return output
    
    def busca_ip(self):
        cadena=self.run_command('hostname -I')
        time.sleep(1)
        if (cadena):
            return cadena.decode()
        else:
            return None
    
    """
    retorna lista de archivos en un directorio, se usa para envio de mediciones guardadas
    """
    def lista_archivos(self,directory):
        stack = [directory]
        files = []
        while stack:
            directory = stack.pop()
            for file in os.listdir(directory):
                fullname = os.path.join(directory, file)
                files.append(fullname)
                if os.path.isdir(fullname) and not os.path.islink(fullname):
                    stack.append(fullname)
        return files
    
    """Retorna la fecha en formato linux para envio de telemetria"""
    def format_linux(self):
        milli_epoch = int(round(time.time() * 1000))
        tiempo_st = time.localtime()
        return milli_epoch
    
    """Retorna la fecha en formato string"""
    def format_string(self):
        now = datetime.now() # current date and time
        year = now.strftime("%Y")
        month = now.strftime("%m")
        day = now.strftime("%d")
        time = now.strftime("%H:%M:%S")
        date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        return (year+month+day)
    
    """Retorna la fecha en formato normal """
    def format_time(self):
        now = datetime.now() # fecha y hora actual
        date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        return (date_time)
    
    """sincroniza reloj"""
    def set_time(self,host = "ntp.shoa.cl"):
        from machine import RTCfrom
        t = ntime(host)
        tm = localtime(t)
        del t
        tm = tm[0:3] + (0,) + tm[3:6] + (0,)
        if tm[0]> 2019:
            RTC().datetime(tm)
            #print("Reloj RTC sincronizado")
            return True
        else:
            return False
        
    def temperatura_cpu(self):
        temp = self.run_command('cat /sys/class/thermal/thermal_zone0/temp')
        temp = round(int(temp)/1000,1)
        tempgpu = self.run_command('vcgencmd measure_temp')
        tempgpu = tempgpu[5:9].decode()
        return (temp,tempgpu)
    
    """
    Clase utilizaria con funciones y servicios de Red Internet y WIFI
    """
class Network_Service():
    utils= Commons()
    
    """Verifica si red esta activa"""
    def internet_on(self):
        try:
            #urllib2.urlopen('http://www.google.com', timeout=1)
            data=self.utils.run_command('ping -c 3 -W 4 '+'8.8.8.8')
            if (len(data) > 120):
                return True
            else:
                return(False)
        except:
            self.utils.write_log("WIFI NOT Connected")
            return False
    
    """Emvia correo con archivo adjunto si esta definido """
    
    def whatsapp(self,cel,mensaje):
        from twilio.rest import Client
        account_sid = 'AC306559b9d5642357003259cb122cc873'
        auth_token = 'eec187b958325b33a74c0fb4ad5cb347'
        
        client = Client(account_sid, auth_token)
        message = client.messages.create( 
                              from_='whatsapp:+14155238886',  
                              body=mensaje,      
                              to='whatsapp:'+cel 
                          )
        return(True)
    
    def send_mail(self,filename,subject,text):
        import smtplib
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.mime.base import MIMEBase
        from email import encoders
        
        conf = self.utils.load_config('nodo.json','Network')
        
        # Iniciamos los parámetros del scrip
        to_mail=str(conf.get('Email_To'))
        from_mail=str(conf.get('Email_From'))
        remitente = str(conf.get('Email_From'))
        destinatarios = [to_mail, 'jorge.diaz@ufrontera.cl']
        #asunto = 'Alerta Sensor Contaminación'
        cuerpo = text
        ruta_adjunto = filename
        nombre_adjunto = filename
        # Creamos el objeto mensaje
        mensaje = MIMEMultipart()
        # Establecemos los atributos del mensaje
        mensaje['From'] = remitente
        mensaje['To'] = ", ".join(destinatarios)
        mensaje['Subject'] = subject
        # Agregamos el cuerpo del mensaje como objeto MIME de tipo texto
        mensaje.attach(MIMEText(cuerpo, 'plain'))
        if (os.path.isfile(filename)):
            # Creamos un objeto MIME base
            adjunto_MIME = MIMEBase('application', 'octet-stream')
            # Abrimos el archivo que vamos a adjuntar
            archivo_adjunto = open(ruta_adjunto, 'rb')
            # Y le cargamos el archivo adjunto
            adjunto_MIME.set_payload((archivo_adjunto).read())
            # Codificamos el objeto en BASE64
            encoders.encode_base64(adjunto_MIME)
            # Agregamos una cabecera al objeto
            adjunto_MIME.add_header('Content-Disposition', "attachment; filename= %s" % nombre_adjunto)
            # Y finalmente lo agregamos al mensaje
            mensaje.attach(adjunto_MIME)
            # Creamos la conexión con el servidor
        try:
            aux=str(conf.get('Server_smtp'))
            sesion_smtp = smtplib.SMTP(aux)
            # Ciframos la conexión
            sesion_smtp.starttls()
            # Iniciamos sesión en el servidor
            aux=str(conf.get('Email_Password'))
            sesion_smtp.login(from_mail,aux)
            # Convertimos el objeto mensaje a texto
            texto = mensaje.as_string()
            # Enviamos el mensaje
            sesion_smtp.sendmail(remitente, destinatarios, texto)
            # Cerramos la conexión
            sesion_smtp.quit()
            self.utils.write_log("Envio email: "+to_mail)
            return(True)
        except:
            self.utils.write_log("NO Envio email (Utiles): " +to_mail)
            return(False)
        
    """Fuerza coneccion a WIFI definida en configuracion"""
    def do_connect(self):
        aux = self.utils.load_config('nodo.json','Network')
        m_wifi = aux.get('Wifi_ID')
        m_wifi_password = str(aux.get('Wifi_Password'))
        try:
            wireless = Wireless()
            intenta=0
            while(self.internet_on()==False and intenta < 6):
                wireless.connect(m_wifi, m_wifi_password)
                self.utils.write_log("Forzando WIFI: "+str(intenta)+ " _ "+m_wifi+m_wifi_password)
                time.sleep(12)
                intenta=intenta+1
            if (self.internet_on()):
                self.utils.write_log("Conección forzada de WIFI")
                return True
            else:
                self.utils.write_log("Error, Conección forzada de WIFI, utiles, do_connect")
                return True
        except:
            if (self.internet_on()):
                return (True)
            else:
                self.utils.write_log("Error, al tratar de conectar WIFI en utiles , do_connect")
                return False
    
    """Envia datos de mediciones en protocolo mqtt con datos en configuracion"""
    
    def atributos(self):
        conf2=self.utils.load_config('nodo.json','Device')
        data1 = {}
        data1['latitud'] = conf2.get('latitude')
        data1['longitud'] = conf2.get('longitude')
        data1['sector'] = conf2.get('area')
        data1['hardware'] = conf2.get('hardware')
        data1['modelo'] = conf2.get('model')
        data1['nombre'] = conf2.get('name')
        data1['version'] = conf2.get('version')
        data1=json.dumps(data1)
        return(data1)
    
    def on_publish(self,client,userdata,result):             #create function for callback
        if (result == 1):
            self.log=self.log+" Atributos OK "
        else:
            if (result == 2):
                self.mal_envio = False
                self.log=self.log+" Telemetria OK "
            else:
                self.log=self.log+"ERROR ERROR"
        pass
    
    def send_mqtt(self,data2,server,token):
        
        ## carga parametros de envio generales asociado al servidor mqtt
        self.log="Envio mqtt SmartAraucania,"
        self.mal_envio = True
        conf1=self.utils.load_config('config.json',server)     
        host   = conf1.get('url')
        port   = conf1.get('mqtt_port')
        interval = conf1.get('keep_alive')
        attribute= conf1.get('attributes_url')
        telemetry= conf1.get('telemetry_url')
        data1=self.atributos()
        if (self.internet_on()):
            client= mqtt.Client()
            client.username_pw_set(token)
            client.on_publish = self.on_publish
            respuesta=client.connect(host,port,interval)
            if respuesta ==0:
                try:
                    client.loop_start()
                    aux1=client.publish(attribute, data1, 1)
                    time.sleep(1)   
                    aux2=client.publish(telemetry, data2, 1)
                    time.sleep(1)
                    client.loop_stop()
                    time.sleep(1)   
                    client.disconnect()
                    time.sleep(1)   
                except:
                    self.utils.write_log("Error, Envio Telemetria MQTT, utiles, send_mqtt, publish")
                    self.utils.write_data(data2)
                    return False
                if (self.mal_envio):
                    self.utils.write_log("Error, fallo envio falso publish utiles, send_mqtt, publish")
                    return False
                else:
                    (t1,t2)=self.utils.temperatura_cpu()
                    self.utils.write_log(self.log+" Temperatura CPU:"+str(t1)+" Temperatura GPU:" +str(t2))
                    return True
            else:
                self.utils.write_log("Error, Fallo en respuesta cliente MQTT, utiles, send_mqtt (guarda medicioes)")
                self.utils.write_data(data2)
                return False
        else:
            self.utils.write_log("Error, Fallo WIFI Envio Telemetria en send_mqtt (guarda medciones)")
            self.utils.write_data(data2)
            return False
    
    """Envia datos de mediciones en protocolo REST con datos en configuracion, se debe verificar"""
    
    def send_http(self,telemetry,server,token):
        conf1=self.utils.load_config('config.json',server)     
        host   = conf1.get('url')
        port   = conf1.get('mqtt_port')
        interval = conf1.get('keep_alive')
        attribute= conf1.get('attributes_url')
        telemetry= conf1.get('telemetry_url')
        ## carga atributos de envio particulares del nodo asociado al servidor mqtt
        conf2=self.utils.load_config('nodo.json','Device')
        data1 = {}
        data1['latitud'] = conf2.get('latitude')
        data1['longitud'] = conf2.get('longitude')
        data1['sector'] = conf2.get('area')
        data1['hardware'] = conf2.get('hardware')
        data1['modelo'] = conf2.get('model')
        data1['nombre'] = conf2.get('name')
        data1['version'] = conf2.get('version')
        data1=json.dumps(data1)
        try:
            self.utils.write_log("Publicando en %s en %s" %(token, host))
            response = requests.post("http://%s:%s/v1/devices/%s/attributes" %(host,port,token), data = data1, headers={"Content-Type": "application/json"})
            self.utils.write_log("P.Atributes: %i" %response.status_code)
            response = requests.post("http://%s:%s/v1/devices/%s/telemetry" %(host,port,token), data = telemetry, headers={"Content-Type": "application/json"})
            self.utils.write_log("P.Telemetry: %i" %response.status_code)
            self.utils.write_log(telemetry)
            if (response.status_code is not 200):
                self.utils.write_log("Error %d en publicacion" %(response.status_code))  
            return True 
        except:
            self.utils.write_log("No fue posible publicar datos de %s en %s debido a: %s" %(token, host))
    
    def envia_registros(self):
        conf=self.utils.load_config('config.json','General')
        conf1=self.utils.load_config('nodo.json','Device')
        files=self.utils.lista_archivos(conf.get('send_path'))
        if (self.internet_on() and files):
            for file in files:
                if os.path.getsize(file) != 0:
                    self.utils.write_log("Enviando mediciones guardadas, envia_registros")
                    archivo=open(file,"r+")
                    regJSON=archivo.readline()
                    auxJSON=json.loads(regJSON)
                    while (regJSON):
                        ret=self.send_mqtt(json.dumps(auxJSON),'Server_1',conf1.get('access_token'))
                        regJSON=archivo.readline()
                        if (regJSON):
                            auxJSON=json.loads(regJSON)
                    archivo.close()
                    if (ret):
                        os.remove(file)
                    else:
                        self.utils.run_command('sudo mv '+file +' /home/pi/sensor/log/'+file[21:])
            return True
        else:
            return False
            
         


    
    

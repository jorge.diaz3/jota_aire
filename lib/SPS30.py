#!/usr/bin/env python3, V1, 
# base code author= Feyzi Kesim , author_email="feyzikesim@gmail.com", url="https://github.com/feyzikesim/sps30"
# Thanks  Feyzi
# this code is change, author= jorge.diaz@ceisufro.cl , is FREE for you
# Jota, versión: 13072021

from smbus2 import SMBus, i2c_msg
import struct
from time import sleep
from lib.utiles import Commons

class SPS30():
    
    log=Commons()
    
    SPS_ADDR = 0x69
    START_MEAS   = [0x00, 0x10]
    STOP_MEAS    = [0x01, 0x04]
    R_DATA_RDY   = [0x02, 0x02]
    R_VALUES     = [0x03, 0x00]
    RW_AUTO_CLN  = [0x80, 0x04]
    START_CLN    = [0x56, 0x07]
    R_ARTICLE_CD = [0xD0, 0x25]
    R_SERIAL_NUM = [0xD0, 0x33]
    RESET        = [0xD3, 0x04]

    NO_ERROR = 1
    ARTICLE_CODE_ERROR = -1
    SERIAL_NUMBER_ERROR = -2
    AUTO_CLN_INTERVAL_ERROR = -3
    DATA_READY_FLAG_ERROR = -4
    MEASURED_VALUES_ERROR = -5

    dict_values = {"pm1p0"  : None,
                   "pm2p5"  : None,
                   "pm4p0"  : None,
                   "pm10p0" : None,
                   "nc0p5"  : None,
                   "nc1p0"  : None,
                   "nc2p5"  : None,
                   "nc4p0"  : None,
                   "nc10p0" : None,
                   "typical": None}

    def __init__(self, port):
        try:
            self.bus = SMBus(port, True) # crea bus I2C y abre comunicación
            self.serial_number = str(self.read_device_serial())
            if self.serial_number == self.ARTICLE_CODE_ERROR:
                self.log.write_log("ARTICLE CODE CRC ERROR!")
        except:
            self.log.write_log("Error al crear objeto SMBUS")

    def calculateCRC(self,input):
        crc = 0xFF
        for i in range (0, 2):
            crc = crc ^ input[i]
            for j in range(8, 0, -1):
                if crc & 0x80:
                    crc = (crc << 1) ^ 0x31
                else:
                    crc = crc << 1
        crc = crc & 0x0000FF
        return crc

    def checkCRC(self,result):
        for i in range(2, len(result), 3):
            data = []
            data.append(result[i-2])
            data.append(result[i-1])
        crc = result[i]
        if crc == self.calculateCRC(data):
            crc_result = True
        else:
            crc_result = False
        return crc_result
    
    def bytes_to_int(self,bytes):
        result = 0
        for b in bytes:
            result = result * 256 + int(b)
        return result
    
    def convertPMValues(self,value):
        string_value = str(hex(value)).replace("0x", "")
        byte_value = bytes.fromhex(string_value)
        return struct.unpack('>f', byte_value)[0]
    
    def read_article_code(self):
        result = []
        article_code = []
        write = i2c_msg.write(self.SPS_ADDR, self.R_ARTICLE_CD)
        self.bus.i2c_rdwr(write)
        read = i2c_msg.read(self.SPS_ADDR, 48)
        self.bus.i2c_rdwr(read)
        for i in range(read.len):
            result.append(self.bytes_to_int(read.buf[i]))
        if self.checkCRC(result):
            for i in range (2, len(result), 3):
                article_code.append(chr(result[i-2]))
                article_code.append(chr(result[i-1]))
            return str("".join(article_code))
        else:
            return self.ARTICLE_CODE_ERROR

    def read_device_serial(self):
        result = []
        device_serial = []

        write = i2c_msg.write(self.SPS_ADDR, self.R_SERIAL_NUM)
        self.bus.i2c_rdwr(write)

        read = i2c_msg.read(self.SPS_ADDR, 48)
        self.bus.i2c_rdwr(read)

        for i in range(read.len):
            result.append(self.bytes_to_int(read.buf[i]))

        if self.checkCRC(result):
            for i in range(2, len(result), 3):
                device_serial.append(chr(result[i-2]))
                device_serial.append(chr(result[i-1]))
            return str("".join(device_serial))
        else:
            return self.SERIAL_NUMBER_ERROR

    def read_auto_cleaning_interval(self):
        result = []

        write = i2c_msg.write(self.SPS_ADDR, self.RW_AUTO_CLN)
        self.bus.i2c_rdwr(write)

        read = i2c_msg.read(self.SPS_ADDR, 6)
        self.bus.i2c_rdwr(read)

        for i in range(read.len):
            result.append(self.bytes_to_int(read.buf[i]))

        if self.checkCRC(result):
            result = result[0] * pow(2, 24) + result[1] * pow(2, 16) + result[3] * pow(2, 8) + result[4]
            return result
        else:
            return self.AUTO_CLN_INTERVAL_ERROR

    def set_auto_cleaning_interval(self, seconds):
        self.RW_AUTO_CLN.append((seconds >> 24) & 0xFF)
        self.RW_AUTO_CLN.append((seconds >> 16) & 0xFF)

        self.RW_AUTO_CLN.append(self.calculateCRC(self.RW_AUTO_CLN[2:4]))

        self.RW_AUTO_CLN.append((seconds >> 8) & 0xFF)
        self.RW_AUTO_CLN.append(seconds & 0xFF)

        self.RW_AUTO_CLN.append(self.calculateCRC(self.RW_AUTO_CLN[5:7]))

        write = i2c_msg.write(self.SPS_ADDR, self.RW_AUTO_CLN)
        self.bus.i2c_rdwr(write)

    def start_fan_cleaning(self):
        write = i2c_msg.write(self.SPS_ADDR, self.START_CLN)
        self.bus.i2c_rdwr(write)
        
    def start_measurement(self):
        try:
            self.START_MEAS.append(0x03)
            self.START_MEAS.append(0x00)
            crc = self.calculateCRC(self.START_MEAS[2:4])
            self.START_MEAS.append(crc)
            write = i2c_msg.write(self.SPS_ADDR, self.START_MEAS)
            self.bus.i2c_rdwr(write)
            sleep(1)
        except:
            self.log.write_log("Error start_measurement ")

    def stop_measurement(self):
        write = i2c_msg.write(self.SPS_ADDR, self.STOP_MEAS)
        self.bus.i2c_rdwr(write)
        
    ## @brief Transforma un arreglo de 4 bytes en un flotante.
    # @param[in] data Arreglo de 4 bytes.
    # @param[out] first Flotante obtenido del arreglo de bytes.
    def _calcFloat(self, data):
        struct_float = pack('>BBBB', data[0], data[1], data[3], data[4])
        float_values = unpack('>f', struct_float)
        first = float_values[0]
        return first

    def read_data_ready_flag(self):
        try:
            result = []
            write = i2c_msg.write(self.SPS_ADDR, self.R_DATA_RDY)
            self.bus.i2c_rdwr(write)
            sleep(1)
            read = i2c_msg.read(self.SPS_ADDR, 3)
            self.bus.i2c_rdwr(read)
            for i in range(read.len):
                result.append(self.bytes_to_int(read.buf[i]))
            if self.checkCRC(result):
                return result[1]
            else:
                self.log.write_log("Error read_data_ready_flag CHECK CRC")
                return self.DATA_READY_FLAG_ERROR
        except:
            self.log.write_log("Error read_data_ready_flag ")
            return self.DATA_READY_FLAG_ERROR

    def read_measured_values(self):
        try:
            result = []
            write = i2c_msg.write(self.SPS_ADDR, self.R_VALUES)
            self.bus.i2c_rdwr(write)
            sleep(1)
            read = i2c_msg.read(self.SPS_ADDR, 60)
            self.bus.i2c_rdwr(read)
            for i in range(read.len):
                result.append(self.bytes_to_int(read.buf[i]))
            if self.checkCRC(result):
                self.parse_sensor_values(result)
                return self.NO_ERROR
            else:
                self.log.write_log("Error read_measured_values checkCRC")
                return self.MEASURED_VALUES_ERROR
        except:
            self.log.write_log("Error read_measured_values ")
            sleep(6)
            #self.full_cleaning()
            return self.MEASURED_VALUES_ERROR

    def device_reset(self):
        try:
            write = i2c_msg.write(self.SPS_ADDR, self.RESET)
            self.bus.i2c_rdwr(write)
            sleep(1)
        except:
            self.log.write_log("Error device_reset ")
            
    
    def full_cleaning(self):
        ### prepara sensor para autolimpiado cada 1 dia y activa el ventilador 2 veces
        self.device_reset()
        sleep(1)
        self.set_auto_cleaning_interval(6600)
        sleep(1)
        if self.read_auto_cleaning_interval() == self.AUTO_CLN_INTERVAL_ERROR: # or returns the interval in secon
            self.log.write_log("AUTO-CLEANING INTERVAL CRC ERROR!")
        else:
            self.log.write_log("AUTO-CLEANING INTERVAL: " + str(self.read_auto_cleaning_interval()))
        try:
            self.start_measurement()
            sleep(3)
            self.start_fan_cleaning() # enables fan-cleaning manually for 10 seconds (referred by datasheet)
            sleep(10)
            self.stop_measurement()
            sleep(2)
            self.start_measurement()
            sleep(3)
            self.start_fan_cleaning()
            sleep(10)
            self.stop_measurement()
            sleep(2)
            self.device_reset()
            return(True)
        except:
            self.log.write_log("Error al limpiar ventilador")
            pass
        return(False)

    def parse_sensor_values(self, input):
        index = 0
        pm_list = []
        for i in range (4, len(input), 6):
            value = input[i] + input[i-1] * pow(2, 8) +input[i-3] * pow(2, 16) + input[i-4] * pow(2, 24)
            pm_list.append(value)

        for i in self.dict_values.keys():
            self.dict_values[i] = self.convertPMValues(pm_list[index])
            index += 1
            
    def appendMeasure(self):
        intentos=0
        while ((self.read_data_ready_flag() == self.DATA_READY_FLAG_ERROR) and intentos < 5):
            intentos+=1
            sleep(1)
        if (intentos > 4):
            self.log.write_log("DATA-READY FLAG CRC ERROR!")
            return(0,0,1)
        if self.read_measured_values() == self.MEASURED_VALUES_ERROR:
            self.log.write_log("MEASURED VALUES CRC ERROR!")
            return (1,1,0)
        else:
            pm25=round(self.dict_values['pm2p5'],1)
            pm10=round(self.dict_values['pm10p0'],1)
            tpsize=round(self.dict_values['typical'],3)
            return (pm25,pm10,tpsize)
    
    def get_telemetry(self):
        conf_sp30=self.log.load_config('config.json','Sp30')
        self.device_reset()
        promedio25=[]
        promedio10=[]
        tpsize=[]
        intentos=0
        mediciones=int(conf_sp30.get('number_med'))
        med_time=int(conf_sp30.get('med_time'))
        self.start_measurement()
        sleep(1)
        while intentos < mediciones:
            (aux1,aux2,aux3)=self.appendMeasure()
            promedio25.append(int(aux1))
            promedio10.append(int(aux2))
            tpsize.append(float(aux3))
            sleep(5)
            intentos+=1
        self.stop_measurement()
        #self.device_reset()
        pm10= round((sum(promedio10)-min(promedio10)-max(promedio10))/(mediciones-2),1)
        pm25= round((sum(promedio25)-min(promedio25)-max(promedio25))/(mediciones-2),1)
        tps= round(sum(tpsize)/len(tpsize),3)
        return(pm10,pm25,tps)
                 

    

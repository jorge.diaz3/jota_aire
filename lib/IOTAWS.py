# this code is change, author= jorge.diaz@ceisufro.cl , is FREE for you
# Jota, versión: 13072021

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient
from board import SCL, SDA
from lib.utiles import Network_Service # libreria con codigo con servicios de red
from lib.utiles import Commons # libreria con codigo de funciones de utilidad general
import logging
import time
import json
import argparse
import busio

"""
Clase Utilitaria para envio al hub IOT de AWS
"""
class IOTAWS:
    com=Commons()
    conf=com.load_config('config.json','Server_AWS')
    conf1=com.load_config('nodo.json','Network')
    conf2=com.load_config('nodo.json','Device')
    
   
    host=conf.get('url')
    port= conf.get('mqtt_port')
    timeout= conf.get('timeout')
    active=conf.get('active')
    privateKeyPath=conf1.get('privateKeyPath')
    rootCAPath=conf1.get('rootCAPath')
    thingName= conf2.get('name')
    certificatePath=conf1.get('certificatePath')
    clientId=conf2.get('name')
    
    
    # respuesta de la publicación de mqtt de AWS
    def customShadowCallback_Update(self,payload, responseStatus, token):
        if responseStatus == "timeout":
            self.com.write_log("Error, publicación IOT AWS timeout")
        if responseStatus == "accepted":
            self.mal_envio = True
        if responseStatus == "rejected":
            self.com.write_log("Error, publicación IOT AWS Update REPETIDA")
        
    # respuesta cuando se elimina una piblicación
    def customShadowCallback_Delete(self,payload, responseStatus, token):
        if responseStatus == "timeout":
            self.com.write_log("Error, borrado publicación IOT AWS timeout")
        if responseStatus == "accepted":
            self.mal_envio = True
        if responseStatus == "rejected":
            self.com.write_log("Error, publicación IOT AWS Update  BORRADA es REENVIADA")
        
    def send_aws(self,payload):
        self.mal_envio = False
        if not self.certificatePath or not self.privateKeyPath:
            self.com.write_log("Error, no se encuentran las credenciales de AWS")
            return (False)
        myAWSIoTMQTTShadowClient = None
        myAWSIoTMQTTShadowClient = AWSIoTMQTTShadowClient(self.clientId)
        myAWSIoTMQTTShadowClient.configureEndpoint(self.host, self.port)
        myAWSIoTMQTTShadowClient.configureCredentials(self.rootCAPath, self.privateKeyPath, self.certificatePath)
        # AWSIoTMQTTShadowClient connection configuration
        myAWSIoTMQTTShadowClient.configureAutoReconnectBackoffTime(1, 32, 20)
        myAWSIoTMQTTShadowClient.configureConnectDisconnectTimeout(self.timeout) # 10 sec
        myAWSIoTMQTTShadowClient.configureMQTTOperationTimeout(5) # 5 sec
        # Connect to AWS IoT
        myAWSIoTMQTTShadowClient.connect()
        # Create a device shadow handler, use this to update and delete shadow document
        deviceShadowHandler = myAWSIoTMQTTShadowClient.createShadowHandlerWithName(self.thingName, True)
        # Delete current shadow JSON doc
        deviceShadowHandler.shadowDelete(self.customShadowCallback_Delete, 5)
        # Update shadow
        deviceShadowHandler.shadowUpdate(json.dumps(payload), self.customShadowCallback_Update, 5)
        time.sleep(1)
        if (self.mal_envio):
            self.com.write_log("Publicación correcta IOT AWS: "+str(payload))
            return(True)
        else:
            self.com.write_log("Error, Publicación IOT AWS: "+str(payload))
            return(False)
        


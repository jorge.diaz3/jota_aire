#!/usr/bin/python3
# Jota, versión: 13072021
import adafruit_dht
import threading
import time
from lib.utiles import Commons

class SDHT22():
    log=Commons()   
    #def __init__(self):
     #   self.name = "DHT22"

    def get_telemetry(self):
        """
        Retorna medición
        """
        conf_dht22=self.log.load_config('config.json','Dht22')
        tipe=conf_dht22.get('Dht_type')
        pin=conf_dht22.get('Dht_pin')
        if (tipe=="DHT22" and pin==4):
            try:
                sensor = adafruit_dht.DHT22(pin)
            except:
                sleep(3)
                self.log.write_log("Error al instanciar DHT22 ")
                sensor = adafruit_dht.DHT22(board.D4)
                pass
            if (sensor):
                temp=0
                hume=0
                intentos = 0
                while (hume==0 and intentos < 5):
                    try:
                        temp = sensor.temperature
                        hume = sensor.humidity
                    except:
                        self.log.write_log("Error al leer datos DHT22: "+str(intentos))
                        pass

                    time.sleep(5)
                    intentos+=1
                del sensor
                return(round(temp-4,1),round(hume+8,1))
            else:
                self.log.write_log(" Error al instanciar DHT22 ")
                return (0,0)
        else:
            self.log.write_log(" Error al instanciar configuracion DHT22 ")
            return (0,0)
        
    def get_temperature(self):
        """
        Returna la temperatura y humedad
        """
        conf_dht22=log.load_config('config.json','Dht22')
        tipe=conf_dht22.get('Dht_type')
        pin=conf_dht22.get('Dht_pin')
        try:
            if (tipe=="DHT22"):
                sensor = adafruit_dht.DHT22(pin, use_pulseio=True)
                temperature = sensor.temperature
                humidity=sensor.humidity
                aux=sensor.exit()
                #print (str(temperature),str(humidity))
                if ((temperature is None) or (int(temperature) > 45) or (int(temperature) < -20)):
                    temperature = 0
                    humidity =0
                return temperature,humidity
        except:
            self.log.write_log("Error al leer DHT22")
            return (0,0)




            
   

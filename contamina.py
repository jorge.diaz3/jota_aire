# Jota, versión: 13072021
import os
import sys
import json
import time
from lib.SPS30   import SPS30 # libreria con codigo de sensor contaminación
from lib.DHT22   import SDHT22 # libreria con codigo de sensor contaminación
from lib.utiles import Network_Service # libreria con codigo con servicios de red
from lib.utiles import Commons # libreria con codigo de funciones de utilidad general
from lib.IOTAWS import IOTAWS # libreria con codigo de funciones de utilidad general

# obtiene los datos de sensores TELEMETRIA
def telemetria():
    com=Commons()
    conf1=com.load_config('nodo.json','Device')
    conf2=com.load_config('nodo.json','Network')
    net=Network_Service()
    
    fecha=com.format_linux()
    try:
        stem=SDHT22()
        (tem,hum)=stem.get_telemetry()
        del stem
        com.write_config('nodo.json','Device','med_tem',int(tem))
        com.write_config('nodo.json','Device','med_hum',int(hum))
        time.sleep(1)
    except:
        com.write_log("Error,  al iniciar SDHT22 en telemetria")
        net.send_mail(" ","Error Sensor de temperatura en telemetria: "+conf1.get('name'),"Error al iniciar sensor de Temperatura")
        tem=int(conf1.get('med_tem'))
        hum=int(conf1.get('med_hum'))
        time.sleep(1)
        pass
    try:
        ssp30=SPS30(1)
        (pm10,pm25,ptsize)=ssp30.get_telemetry()
        del ssp30
        if (int(pm25)<2):
            pm25=2
            pm10=3  
        com.write_config('nodo.json','Device','med_pm25',int(pm25))
        com.write_config('nodo.json','Device','med_pm10',int(pm10))
        #com.write_config('nodo.json','Device','med_size',ptsize)
        com.write_config('nodo.json','Device','nmediciones',int(conf1.get('nmediciones')+1))
        com.write_config('nodo.json','Device','smediciones',int(conf1.get('smediciones')+int(pm25)))
        
        pass
    except:
        com.write_log("Error, al iniciar SPS30 en telemetria")
        net.send_mail("","Error, al iniciar sensor, telemetria: "+conf1.get('name'),"Error sensor de contaminación")
        pm25=int(conf1.get('med_pm25'))
        pm10=int(conf1.get('med_pm10'))
        ptsize=1
        pass
    
    text= " Mediciones Actuales: "+conf1.get('name')+" PM2.5:"+str(pm25)+"-"+" PM10:"+str(pm10)+"-"+" Temperatura: "+str(tem)+"-"+" Humedad: "+str(hum)+" Tamaño: "+str(ptsize)
    # comentar la siguiente linea cuando el sistema este estable
    com.write_log(text)
    
    if (int(conf1.get('nmediciones'))>4):
        promedio= round(int(conf1.get('smediciones')/5))
        com.write_config('nodo.json','Device','nmediciones',0)
        com.write_config('nodo.json','Device','smediciones',0)
        if (promedio>int(conf2.get('number_alert'))):
            try:
                net.whatsapp(conf1.get('celular'),"Promedio última hora PM2_5: "+str(promedio)+ text)
                net.send_mail("","Alerta Sensor: "+conf1.get('name'),"Promedio hora PM2_5: "+str(promedio)+text)
                del net
            except:
                com.write_log("Error, No se pudo enviar correo de alerta contaminación, telemetria")
                pass
    sensor_data2 = {}
    sensor_data2['medicion:contaminante:mp2_5'] = pm25
    sensor_data2['fecha:contaminante:mp2_5'] = fecha
    sensor_data2['modelo'] = conf1.get('model')
    sensor_data2['medicion:contaminante:mp10'] = pm10
    sensor_data2['fecha:contaminante:mp10'] = fecha
    sensor_data2['medicion:meteorologia:temperatura'] = tem
    sensor_data2['fecha:meteorologia:temperatura'] = fecha
    sensor_data2['medicion:meteorologia:humedad'] = hum
    sensor_data2['fecha:meteorologia:humedad'] = fecha
    
    # prepara datos para enviar a AWS
    sensor_data1 = {"state":{"reported":{"medicion25":int(pm25),"medicion10":int(pm10),"medicionsize":ptsize,"mediciontemperatura":int(tem),"medicionhumedad":int(hum)}}}
    #sensor_data1 = {"medicion25":int(pm25),"medicion10":int(pm10),"medicionsize":ptsize,"mediciontemperatura":int(tem),"medicionhumedad":int(hum)}
    return (sensor_data2, sensor_data1)
    
def inicio():
    com = Commons()
    net = Network_Service()
    conf = com.load_config('config.json','General')
    conf1= com.load_config('nodo.json','Device')
    conf2 = com.load_config('nodo.json','Network')
    
    if (not net.internet_on()):
        if (not net.do_connect()):
            com.write_log("Error, No se pudo iniciar WIFI en inicio")
    
    if (os.path.isfile('/home/pi/sensor/firt_run.now') != 0):
        os.remove('/home/pi/sensor/firt_run.now')
        new_ip=str(com.busca_ip())# actualiza el ip
        time.sleep(2)
        net.whatsapp(str(conf1.get('celular')),'Reinicio normal nodo ip: '+new_ip+" "+str(conf1.get('name')))
        ssp30=SPS30(1)
        ssp30.full_cleaning()
        del ssp30
        time.sleep(2)
        if (new_ip != None):
            com.write_config('nodo.json','Device','ip_local',new_ip)
        else:
            new_ip=str(conf1.get('ip_local'))
        time.sleep(2)
        net.send_mail("/home/pi/sensor/log/log.txt","Log Sensor IP: "+new_ip+" - "+str(conf1.get('name')),"Envio Log Sensor")
        os.remove("/home/pi/sensor/log/log.txt")
        if (conf1.get('energia')):
            com.run_command('sudo /opt/vc/bin/tvservice -o')
            com.run_command('dtparam=act_led_trigger=none')
            com.run_command('echo ''1-1'' |sudo tee /sys/bus/usb/drivers/usb/unbind')
            
    if (com.lista_archivos(conf.get('send_path'))): # verifica si hay mediciones grabadas para enviar
        net.envia_registros() # envia o guarda mediciones almacenadas en carpeta END

#     comentar la siguiente línea cuando el sistema este estable, envia log por email
    if (os.path.isfile('reboot.now') != 0):
        os.remove("/home/pi/sensor/reboot.now")
    time.sleep(1)
    del com
    del net
    return (True)
    
    

######## main control ########

com = Commons()
iotAWS=IOTAWS()
net = Network_Service()
conf = com.load_config('config.json','General')
conf1= com.load_config('nodo.json','Device')
conf2 = com.load_config('nodo.json','Network')

inicio() ## llama a verificaciones del inicio

#     inicio de lectura
try:
    (data1,data2)=telemetria() # se obtienen JSON con mediciones
except:
    com.write_log("Error, al llamar a telemetria en main")
    net.send_mail(" ","Error telemetria en main: "+conf1.get('name'),"Error al llamar a telemetria")
    time.sleep(10)
    if (os.path.isfile('reboot.now') != 0):
        net.send_mail(" ","Error de WIFI reboot realizado: "+conf1.get('name'),"Error, WIFI, Reboot")
    else:
        com.run_command('sudo touch reboot.now')
        com.write_log("Error, lectura telemetria, REBOOT")
        com.run_command('sudo reboot')
 
# se trata de enviar los datos al servidor

if (net.internet_on()):
    try:
        aux=net.send_mqtt(json.dumps(data1),'Server_1',conf2.get('access_token'))
        if (iotAWS.active == 'True'):
            if (not iotAWS.send_aws(data2)):
                com.write_log("Error, al enviar datos al servidor IOT AWS en main")
    except:
        com.write_log("Error, al enviar datos al servidor en main")
        net.send_mail("","Error al enviar datos al servidor, main: "+conf1.get('name'),"Error al enviar datos al servidor")
        pass
else:
    com.write_log("Error, sin WIFI para enviar en main")
    if (net.do_connect()):
        aux=net.send_mqtt(json.dumps(data1),'Server_1',conf2.get('access_token'))
        if (not iotAWS.send_aws(data2)):
            com.write_log("Error, al enviar datos al servidor IOT AWS en main")
    else:
        time.sleep(10)
        com.write_log("Error, sin WIFI despues de tratar de conectar en main, no logra enviar, REBOOT")
        aux=net.send_mqtt(json.dumps(data1),'Server_1',conf2.get('access_token')) # trata de enviar para guardar
        com.run_command('sudo touch reboot.now') # genera un flag para no quedar loop en reboot, 
        time.sleep(3)
        com.run_command('sudo reboot')#  reboot para trata de conectar

## revisa temperatura cpu para alerta por whatsapp
        
(t1,t2)=com.temperatura_cpu()

if (int(t1) > 61):
    net.whatsapp(conf1.get('celular'),"Alerta Temperatura sensor "+ str(conf1.get('name'))+": CPU= "+str(t1)+" GPU: "+str(t2))
    net.send_mail("","Alerta Temperatura CPU nodo: "+conf1.get('name'), "Alerta Temperatura sensor CPU= "+str(t1)+" GPU: "+str(t2))


    

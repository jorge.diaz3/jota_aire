# Jota, versión: 13072021, v1
import subprocess
import os

def run_command(cmd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    output, err = proc.communicate()
    del proc
    return output
    
######## main 

print("Configuración Nodo Sensor Contaminación, Versión BETA 24062021:\n")

print ("Actualizando Sistema, ESPERE MUCHOS MINUTOS")
aux=run_command('sudo apt update') 
print (aux.decode())

aux=run_command('sudo apt upgrade -y') 
print (aux.decode())

print ("\nInstalando Librerias Python3\n")

aux=run_command('sudo apt install python3-pip libgpiod2') 
print (aux.decode())

aux=run_command('sudo pip3 install AWSIoTPythonSDK twilio crcmod paho-mqtt adafruit-circuitpython-dht requests urllib3 wireless packaging wifi GPIO python-crontab smbus2')
print (aux.decode())

aux=run_command('cd /home/pi/sensor') 

aux=run_command('git config --global user.email sensor.lazos@gmail.com')
aux=run_command('git config --global user.name PI')
print (aux.decode())

import json
from crontab import CronTab
from lib.utiles import Commons
from lib.utiles import Network_Service
from lib.SPS30   import SPS30
from lib.DHT22   import SDHT22

#### configutación archivo nodo.json con datos del nodo local
com=Commons() 
net=Network_Service()
conf1 = com.load_config('nodo.json','Device')
conf2 = com.load_config('nodo.json','Network')

print ("\nEscribiendo Configuración Base:\n")
if (not os.path.isfile('/home/pi/sensor/nodo.json')):
    config = {"Device": {"hardware": "Raspberry PI 3B", "latitude": "-38.73095", "longitude": "-72.6453138888889", "celular": "+56999975194", "model": "sps30", "sensor1": "sps30", "sensor2": "DHT22", "sensor3": "MG80", "name": "Casa Claudio ", "area": "poniente", "type": "USB Raspberry", "version": "24042021", "uid": "jota-002", "info-extra": "{}", "med_tem": 21, "med_hum": 64, "med_pm25": 6, "med_pm10": 8, "ip_local": "192.168.1.130"}, "Network": {"Email_From": "sensor.lazos@gmail.com", "Email_Password": "sensor.2021", "Email_To": "jorge.diaz@lazos.cl", "Wifi_ID": "WifiTelsur_2.4", "Wifi_Password": "c3d4e5", "Server_smtp": "smtp.gmail.com:587", "number_alert": 120, "access_token": "Ys6QxEUCrbSbPKfYZ2Kl"}}
    with open('/home/pi/sensor/nodo.json', 'w') as f:
        f.write(json.dumps(config))
        f.close()          
hard=0
while (hard != 1 and hard != 2 and hard != 3):
    print ("\n\n Configuración Local Sensor\n\n")
    hard=input("Tipo de Controlador Raspberry, ingrese número (1: PI 3A, 2: PI 3B, 3: PI 4:)=")
    hard=int(hard)    
if (hard==1):
    hard="Raspberry PI 3A"
if (hard==2):
    hard="Raspberry PI 3B"
if (hard==3):
    hard="Raspberry PI 4"
com.write_config('nodo.json','Device','hardware',hard)

stem=0
while (stem != 1 and stem != 2):
    stem=input("Tipo de Sensor de Temperatura, ingrese número (1: DHT22, 2: BME280)=")
    stem=int(stem)    
if (stem==1):
    stem="DHT22"
if (stem==2):
    stem="BME280"

com.write_config('nodo.json','Device','sensor2',stem)
name=input("Ingrese Nombre del Nodo, valor actual=" + str(conf1.get('name'))+ ":")
area=input("Ingrese Nombre de la Área, valor actual="+str(conf1.get('area'))+":")
lat=input("Ingrese Latitud, valor actual="+str(conf1.get('latitude'))+":")
log=input("Ingrese Longitud,valor actual="+str(conf1.get('longitude'))+":")
token=input("Ingrese Token al servidor SmartAraucania,valor actual="+str(conf2.get('access_token'))+":")
aux=conf2.get('certificatePath')
if (aux):
    aux=aux[20:30]
else:
    aux="NADA"
tokenAWS=input("Ingrese Token nombre de archivo seguridad AWS IOT="+aux+":")
if (not tokenAWS):
    tokenAWS=aux
mail1="sensor.lazos@gmail.com"
passwd="sensor.2021"
mail2=input("Ingrese Correo para avisos,valor actual="+str(conf2.get('Email_To'))+":")
max2=input("Ingrese umbral de aviso PM 2.5,valor actual="+str(conf2.get('number_alert'))+":")
med=input("Ingrese intervalo en minutos para hacer mediciones,defecto="+"12:")
celular=str(conf1.get('celular'))
cel=input("Ingrese numero de celular para whatsapp,valor actual="+celular+":")

if (not med):
    med=12

wifi=input("Ingrese WIFI AP ID, valor actual="+conf2.get('Wifi_ID')+":")
wifi_password=input("Ingrese Password WIFI, valor actual="+conf2.get('Wifi_Password')+":")

if (name):
    com.write_config('nodo.json','Device','name',name)
if (area):
    com.write_config('nodo.json','Device','area',area)
if (lat):
    com.write_config('nodo.json','Device','latitude',lat)
if (log):
    com.write_config('nodo.json','Device','longitude',log)
if (token):
    com.write_config('nodo.json','Network','access_token',token)
if (mail1):
    com.write_config('nodo.json','Network','Email_From',mail1)
if (passwd):
    com.write_config('nodo.json','Network','Email_Password',passwd)
if (mail2):
    com.write_config('nodo.json','Network','Email_To',mail2)
if (max2):
    com.write_config('nodo.json','Network','number_alert',int(max2))
if (wifi):
    com.write_config('nodo.json','Network','Wifi_ID',wifi)
if (wifi_password):
    com.write_config('nodo.json','Network','Wifi_Password',wifi_password)
if (cel):   
    com.write_config('nodo.json','Device','celular',cel)
    celular=cel

com.write_config('nodo.json','Network','Server_smtp',"smtp.gmail.com:587")
com.write_config('nodo.json','Device','sensor1',"sps30")
com.write_config('nodo.json','Device','sensor3',"")
com.write_config('nodo.json','Device','type',"USB Raspberry")
com.write_config('nodo.json','Device','version', "24062021")
com.write_config('nodo.json','Device','uid',"jota-002")
com.write_config('nodo.json','Device','ip_local',com.busca_ip())
com.write_config('nodo.json','Device','info-extra',"{}")
com.write_config('nodo.json','Device','energia',1)


# aws

com.write_config('nodo.json','Network','certificatePath',"/home/pi/sensor/AWS/"+tokenAWS+".cert.pem")
com.write_config('nodo.json','Network','privateKeyPath',"/home/pi/sensor/AWS/"+tokenAWS+".private.key")
com.write_config('nodo.json','Network','rootCAPath',"/home/pi/sensor/AWS/AmazonRootCA1.cer")
com.write_config('nodo.json','Network','clientId',"basicShadowUpdater")
com.write_config('nodo.json','Network','thingName',"Bot")
com.write_config('nodo.json','Network','timeout',10)



if (wifi):
    com.write_wifi(wifi,wifi_password)

#### configutación del crontab para llamada automatica

cron = CronTab(user='pi')
cron.remove_all()
job = cron.new(command='/bin/sh /home/pi/sensor/run.sh > /home/pi/sensor/log/salida_contamina.txt 2>&1')
job.minute.every(int(med))
job = cron.new(command='/bin/sh /home/pi/sensor/update.sh > /home/pi/sensor/log/salida_update.txt 2>&1')
job.setall('15 6,14,22 * * *')
cron.write()

#####  verificación de sensores

print ("Verificando sensor de Temperatura:")
stem=SDHT22()
(tem,hum)=stem.get_telemetry()
if (tem and hum):
    print ("Sensor de temperatura Correcto: "+str(tem)+" -- "+str(hum))
    del stem

print ("Verificando sensor de Contaminación:")
ssp30=SPS30(1)

if (ssp30.serial_number):
    print ("Sensor de contaminación Correcto, serial number: "+str(ssp30.serial_number))
    del ssp30


#####  envio correo con la configuración para acceso ss

conf1 = com.load_config('nodo.json','Device')
conf2 = com.load_config('nodo.json','Network')

subject= 'nuevo sensor ' + str(conf1.get('name'))

conf1=json.dumps(conf1, sort_keys=True, indent=4)
conf2=json.dumps(conf2, sort_keys=True, indent=4)
aux= "Instalado Nuevo Sensor de Contaminación: \n"+str(conf1)+"\n"+str(conf2)
net.send_mail("",subject,str(aux))

net.whatsapp(celular,str(aux))

print ("Enviado Correo y whatsapp con configuración:")

print ("Fin de configuración")





